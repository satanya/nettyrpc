package com.newlandframework.rpc.services.pojo;

import java.io.Serializable;

/**
 * @program: ServerPerson
 * @author: 2K
 * @create: 2019-03-11
 **/


public class ServerPerson implements Serializable {

    public int port;
    public String command_type;
    public String command;

    public void setPort(int port) {
        this.port = port;
    }

    public void setCommand_type(String command_type) {
        this.command_type = command_type;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getPort() {
        return port;
    }

    public String getCommand_type() {
        return command_type;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return "ServerPerson [port=" + port + ", command_type=" + command_type + ", command=" + command + "]";
    }
}
